module bitbucket.org/uwaploe/ccdmon

go 1.13

require (
	bitbucket.org/uwaploe/pducom v0.9.3
	github.com/coreos/go-systemd/v22 v22.0.0
	github.com/gomodule/redigo v2.0.0+incompatible
)
