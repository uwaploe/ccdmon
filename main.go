// Enable the imagerpc server when the CCD is powered on
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/pducom"
	"github.com/coreos/go-systemd/v22/dbus"
	"github.com/gomodule/redigo/redis"
)

const Usage = `Usage: ccdmon [options]

Start or stop the Image Acquisition server when the CCD is powered on or off.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr  string = "localhost:6379"
	rdChan  string = "data.pdu.POWER"
	svcName string = "imagerpc.service"
)

// Power status data records
type dataRecord struct {
	T    time.Time    `json:"time"`
	Src  string       `json:"source"`
	Data pducom.Power `json:"data"`
}

// Pass all received Redis pub-sub messages to a Go channel.
func pubsubReader(psc redis.PubSubConn, qlen int) <-chan redis.Message {
	c := make(chan redis.Message, qlen)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				select {
				case c <- msg:
				default:
					log.Printf("Queue full, %q message dropped", msg.Data)
				}
			}
		}
	}()

	return c
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "rdaddr",
		lookupEnvOrString("PDU_REDIS", rdAddr),
		"Redis host:port for message publishing")
	flag.StringVar(&rdChan, "rdchan",
		lookupEnvOrString("POWER_CHAN", rdChan),
		"Redis channel for PDU power messages")
	flag.StringVar(&svcName, "svc",
		lookupEnvOrString("IA_SERVICE", svcName),
		"Name of Image Acquisition service file")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func isActive(conn *dbus.Conn, name string) bool {
	status, err := conn.ListUnitsByNames([]string{name})
	if err != nil {
		log.Printf("Cannot check status: %v", err)
		return false
	}
	return status[0].ActiveState == "active"
}

func maybeStart(conn *dbus.Conn, name string, result chan string) {
	if isActive(conn, name) {
		return
	}
	_, err := conn.StartUnit(name, "replace", result)
	if err != nil {
		log.Printf("Cannot start %q: %v", name, err)
	}
}

func maybeStop(conn *dbus.Conn, name string, result chan string) {
	if !isActive(conn, name) {
		return
	}
	_, err := conn.StopUnit(name, "replace", result)
	if err != nil {
		log.Printf("Cannot stop %q: %v", name, err)
	}
}

func main() {
	parseCmdLine()

	conn, err := dbus.NewSystemConnection()
	if err != nil {
		log.Fatalf("Cannot establish Systemd connection: %v", err)
	}

	// Redis connection for the pub-sub subscription
	rdconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis: %v", err)
	}
	defer rdconn.Close()
	psc := redis.PubSubConn{Conn: rdconn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	ch := pubsubReader(psc, 1)
	psc.Subscribe(rdChan)
	start_ch := make(chan string, 1)
	stop_ch := make(chan string, 1)

	log.Printf("CCD monitor started (%s)", Version)

	for {
		select {
		case result := <-start_ch:
			log.Printf("Start operation complete: %q", result)
		case result := <-stop_ch:
			log.Printf("Stop operation complete: %q", result)
		case msg, ok := <-ch:
			if !ok {
				conn.Close()
				return
			}
			rec := dataRecord{}
			json.Unmarshal(msg.Data, &rec)
			state, ok := rec.Data["CCD"]
			if !ok {
				continue
			}
			if state {
				maybeStart(conn, svcName, start_ch)
			} else {
				maybeStop(conn, svcName, stop_ch)
			}
		}
	}
}
